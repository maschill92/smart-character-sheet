module.exports = {
    backgrounds: [
        {
            id: 1,
            name: 'Acolyte'
        },
        {
            id: 2,
            name: 'Charlatan'
        },
        {
            id: 3,
            name: 'Criminal'
        },
        {
            id: 4,
            name: 'Entertainer'
        },
        {
            id: 5,
            name: 'Folk Hero'
        },
        {
            id: 6,
            name: 'Guild Artisan'
        },
        {
            id: 7,
            name: 'Hermit'
        },
        {
            id: 8,
            name: 'Noble'
        },
        {
            id: 9,
            name: 'Outlander'
        },
        {
            id: 10,
            name: 'Sailor'
        },
        {
            id: 11,
            name: 'Soldier'
        },
        {
            id: 12,
            name: 'Urchin'
        }
    ],
    classes: [
        {
            id: 1,
            name: 'Barbarian'
        },
        {
            id: 2,
            name: 'Bard'
        },
        {
            id: 3,
            name: 'Cleric'
        },
        {
            id: 4,
            name: 'Druid'
        },
        {
            id: 5,
            name: 'Fighter'
        },
        {
            id: 6,
            name: 'Monk'
        },
        {
            id: 7,
            name: 'Paladin'
        },
        {
            id: 8,
            name: 'Ranger'
        },
        {
            id: 9,
            name: 'Rogue'
        },
        {
            id: 10,
            name: 'Sorcerer'
        },
        {
            id: 11,
            name: 'Warlock'
        },
        {
            id: 12,
            name: 'Wizard'
        }
    ],
    races: [
        {
            id: 1,
            name: 'Dwarf',
            hasSubrace: true,
            parent: null,
            abilityScoreBonuses: {
                strength: 0,
                constitution: 2,
                dexterity: 0,
                intelligence: 0,
                wisdom: 0,
                charisma: 0
            },
            size: 'medium',
            speed: 25,
            darkvision: 60,
            skillProficienciesGranted: [],
            weaponProficienciesGranted: ['battleaxe', 'handaxe', 'light hammer', 'warhammer'],
            armorProficienciesGranted: ['test'],
            languagesGranted: ['common', 'dwarvish'],
            languagesOptionCount: 0,
            features: [
                {
                    id: 1,
                    name: 'Dwarven Resilience',
                    description: 'You have advantage on saving throws against poison, and you have resistance against ' +
                    'poison damage.'
                },
                {
                    id: 2,
                    name: 'Stonecunning',
                    description: 'Whenever you make an Intelligence (History) check related to the origin of stonework, ' +
                    'you are considered proficient in the History skill and add double your proficiency bonus to the ' +
                    'check, instead of you normal proficiency bonus.'
                }
            ],
            toolProficiencyOptions: {
                count: 1,
                list: ['smith\'s tools', 'brewer\'s supplies', 'mason\'s tools']
            },
            racialSpellOptions: {},
            rules: {}
        },
        {
            id: 2,
            name: 'Hill Dwarf',
            hasSubrace: false,
            parent: 1,
            abilityScoreBonuses: {
                strength: 0,
                constitution: 0,
                dexterity: 0,
                intelligence: 0,
                wisdom: 1,
                charisma: 0
            },
            rules: {
                1: {hitPointsGranted: 1},
                2: {hitPointsGranted: 1},
                3: {hitPointsGranted: 1},
                4: {hitPointsGranted: 1},
                5: {hitPointsGranted: 1},
                6: {hitPointsGranted: 1},
                7: {hitPointsGranted: 1},
                8: {hitPointsGranted: 1},
                9: {hitPointsGranted: 1},
                10: {hitPointsGranted: 1},
                11: {hitPointsGranted: 1},
                12: {hitPointsGranted: 1},
                13: {hitPointsGranted: 1},
                14: {hitPointsGranted: 1},
                15: {hitPointsGranted: 1},
                16: {hitPointsGranted: 1},
                17: {hitPointsGranted: 1},
                18: {hitPointsGranted: 1},
                19: {hitPointsGranted: 1},
                20: {hitPointsGranted: 1},
            }
        },
        {
            id: 3,
            name: 'Mountain Dwarf',
            hasSubrace: false,
            parent: 1,
            abilityScoreBonuses: {
                strength: 2,
                constitution: 0,
                dexterity: 0,
                intelligence: 0,
                wisdom: 0,
                charisma: 0
            },
            armorProficienciesGranted: ['light', 'medium'],
        },
        {
            id: 4,
            name: 'Human',
            hasSubrace: false,
            abilityScoreBonuses: {
                strength: 1,
                constitution: 1,
                dexterity: 1,
                intelligence: 1,
                wisdom: 1,
                charisma: 1
            },
            size: 'medium',
            speed: 30,
            darkvision: 0,
            skillProficienciesGranted: [],
            weaponProficienciesGranted: [],
            armorProficienciesGranted: [],
            languagesGranted: ['common'],
            languagesOptionCount: 1,
            features: [],
            toolProficiencyOptions: {},
            racialSpellOptions: {},
            rules: {}
        }
    ]
};