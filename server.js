var webpack = require('webpack');
var webpackDevMiddleware = require('webpack-dev-middleware');
var webpackConfig = require('./webpack.config');

var app = require('express')();
var port = 3000;

var compiler = webpack(webpackConfig);

// disable all caching for now
app.use(function (req, res, next) {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '-1');
    res.header('Pragma', 'no-cache');
    next()
});

app.use(webpackDevMiddleware(compiler, { noInfo: false, publicPath: webpackConfig.output.publicPath}));

var data = require('./characterCreatorData');
app.use('/api/characterCreator/races', function(req, res) {
    res.json(data.races);
});
app.use('/api/characterCreator/backgrounds', function(req, res) {
    res.json(data.backgrounds);
});
app.use('/api/characterCreator/classes', function(req, res) {
    res.json(data.classes);
});

app.use(function(req, res) {
    res.sendFile(__dirname + '/client/index.html');
});

app.listen(port,  function(error) {
    if (error) {
        console.error(error)
    } else {
        console.info("==> Listening on port %s. Open up http://localhost:%s/ in your browser.", port, port)
    }
});