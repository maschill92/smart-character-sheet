import React from 'react'
import {Route, IndexRoute, IndexRedirect} from 'react-router'

import App from './containers/App'
import CharacterList from './characterList/components/CharacterList'
import CharacterCreator from './characterCreator/containers/CharacterCreator'

export default (
    <Route path="/" component={App}>
        <IndexRedirect to="characters"/>
        <Route path="characters">
            <IndexRoute component={CharacterList}/>
            <Route path="create" component={CharacterCreator}/>
        </Route>
    </Route>
)