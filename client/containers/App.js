import React from 'react';
import {Link, IndexLink} from 'react-router'

const App = ({children}) => (
    <div>
        <nav className="navbar navbar-full navbar-dark bg-inverse">
            <div>
                <button className="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse"
                        data-target="#collapsingNavbar">&#9776;</button>
            </div>
            <div className="collapse navbar-toggleable-xs" id="collapsingNavbar">
                <Link to="/" className="navbar-brand">Character Sheet</Link>
                <ul className="nav navbar-nav">
                    <li className="nav-item">
                        <IndexLink to="/characters/" className="nav-link" activeClassName="active">My Characters</IndexLink>
                    </li>
                    <li className="nav-item">
                        <Link to="/characters/create" className="nav-link" activeClassName="active">Create Character</Link>
                    </li>
                </ul>
            </div>
        </nav>
        {children}
    </div>
);

export default App;
