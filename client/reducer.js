import {combineReducers} from 'redux'
import {routerReducer as routing} from 'react-router-redux'
import {reducer as formReducer} from 'redux-form'

import {reducer as characterCreator} from './characterCreator'

export default combineReducers({
    // application reducers
    characterCreator,

    // third party reducers
    form: formReducer,
    routing
});