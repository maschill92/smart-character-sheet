import React, {PropTypes} from 'react'
import {Field} from 'redux-form'

const renderFormField = ({label, hideLabel, type, input, meta, children}) => {
    const showError = meta.touched && meta.error;
    let field;
    if (type === 'select') {
        field = ( <select {...input} className="form-control">{children}</select> );
    } else {
        field = ( <input {...input} className="form-control" type={type}/> );
    }

    return (
        <div className={'form-group ' + (showError ? 'has-danger' : '')}>
            <label htmlFor="name" className={hideLabel ? 'sr-only' : ''}>{label}</label>
            {field}
            <span className="form-control-feedback">{showError && meta.error}</span>
        </div>
    );
};

const FormField = (props) => (
    <Field {...props} component={renderFormField}/>
);

FormField.defaultProp = {
    hideLabel: false
};

FormField.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    hideLabel: PropTypes.bool,
    type: PropTypes.string.isRequired
};

export default FormField;