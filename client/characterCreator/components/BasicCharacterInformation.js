import React from 'react'
import SimpleFormField from './SimpleFormField'

const BasicCharacterInformationForm = () => (
    <div className="card card-block">
        <h4 className="card-title font-weight-normal">Basic Information</h4>
        <div className="row">
            <div className="col-xs-12 col-md-6">
                <SimpleFormField name="name" label="Name" type="text"/>
            </div>
            <div className="col-xs-6 col-md-3">
                <SimpleFormField name="playerName" label="Player Name" type="text"/>
            </div>
            <div className="col-xs-6 col-md-3">
                <SimpleFormField name="alignment" label="Alignment" type="select">
                    <option value="LG">Lawful Good</option>
                    <option value="NG">Good</option>
                    <option value="CG">Chaotic Good</option>
                    <option value="LN">Lawful Neutral</option>
                    <option value="NN">Neutral</option>
                    <option value="CN">Chaotic Neutral</option>
                    <option value="LE">Lawful Evil</option>
                    <option value="NE">Evil</option>
                    <option value="CE">Chaotic Evil</option>
                </SimpleFormField>
            </div>
        </div>

        <div className="row">
            <div className="col-xs-6 col-md-3">
                <SimpleFormField name="age" label="Age" type="text"/>
            </div>
            <div className="col-xs-6 col-md-3">
                <SimpleFormField name="sex" label="Sex" type="text"/>
            </div>
            <div className="col-xs-6 col-md-3">
                <SimpleFormField name="height" label="Height" type="text"/>
            </div>
            <div className="col-xs-6 col-md-3">
                <SimpleFormField name="weight" label="Weight" type="text"/>
            </div>
        </div>
    </div>
);

export default BasicCharacterInformationForm;