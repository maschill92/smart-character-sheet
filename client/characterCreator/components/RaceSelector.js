import React, {Component, PropTypes} from 'react'
import _ from 'lodash'

const generateListOfRaceOptions = (races) => {
    if (races.length > 0) {
        return races.map((race) => (
            <option key={race.id} value={race.id}>{race.name}</option>
        ));
    }
};

/**
 * Returns the subrace with data from race applied to it. If subrace is not supplied, race is returned.
 *
 * @param race Root race, required
 * @param subrace Subrace, optional
 */
const mergeRaceAndSubrace = (race, subrace) => {
    if (!subrace) {
        return race;
    }
    return _.mergeWith({}, race, subrace, (objValue, srcValue, key, object, source) => {
        // do custom merging for abilityScoreBonuses by adding the parent and subrace values.
        if (key === 'abilityScoreBonuses' && source.parent !== null) {
            return {
                strength: objValue.strength + srcValue.strength,
                constitution: objValue.constitution + srcValue.constitution,
                dexterity: objValue.dexterity + srcValue.dexterity,
                intelligence: objValue.intelligence + srcValue.intelligence,
                wisdom: objValue.wisdom + srcValue.wisdom,
                charisma: objValue.charisma + srcValue.charisma,
            }
        }
    });
};

/**
 * This class knows it will be used within a 'redux-form' context. The library passes information and callbacks via
 * props.input.
 */
class RaceSelector extends Component {
    constructor(props) {
        super(props);
        this.handleRaceSelectChange = this.handleRaceSelectChange.bind(this);
        this.handleSubraceSelectChange = this.handleSubraceSelectChange.bind(this);
    }

    componentWillMount() {
        // state initialization
        // if this field is initialized with a value, the proper
        // race and subrace ids will be set on this components state
        let selectedRace = null;
        let selectedRaceId = null;
        let selectedSubraceId = null;
        const value = this.props.input.value ? this.props.input.value : null;
        if (value != null) {
            selectedRace = this.props.races.find((race) => race.id === value);
            if (selectedRace.parent) {
                selectedRaceId = selectedRace.parent;
                selectedSubraceId = selectedRace.id;
            } else {
                selectedRaceId = selectedRace.id;
                selectedSubraceId = null;
            }

            this.props.onRaceSelectionComplete(mergeRaceAndSubrace(this.findRaceById(selectedRaceId), this.findRaceById(selectedSubraceId)));
        }

        this.setState({
            selectedRaceId,
            selectedSubraceId
        });
    }

    handleRaceSelectChange(event) {
        const selectedRaceId = Number.parseInt(event.target.value) || null;
        if (selectedRaceId === null) {
            this.setState({
                selectedRaceId: null,
                selectedSubraceId: null
            });
            this.props.onRaceSelectionComplete(null);
            return;
        }

        const selectedRace = this.findRaceById(selectedRaceId);
        if (selectedRace && selectedRace.hasSubrace) {
            // default to the first
            const selectedSubrace = this.props.races.find((race) =>  race.parent === selectedRaceId);
            this.props.onRaceSelectionComplete(mergeRaceAndSubrace(selectedRace, selectedSubrace));
            this.setState({
                selectedRaceId,
                selectedSubraceId: selectedSubrace.id
            });
        } else {
            this.props.onRaceSelectionComplete(selectedRace);
            this.setState({
                selectedRaceId,
                selectedSubraceId: null
            });
        }
    }

    handleSubraceSelectChange(event) {
        const selectedSubraceId = Number.parseInt(event.target.value) || null;
        if (selectedSubraceId === null) {
            this.setState({
                selectedRaceId: null,
                selectedSubraceId: null
            });
            this.props.onRaceSelectionComplete(null);
        }

        const selectedSubrace = this.findRaceById(selectedSubraceId);
        const selectedRace = this.findRaceById(selectedSubrace.parent);
        const mergedRace = mergeRaceAndSubrace(selectedRace, selectedSubrace);

        this.setState({
            selectedRaceId: selectedSubrace.parent,
            selectedSubraceId
        });
        this.props.onRaceSelectionComplete(mergedRace);

    }

    finalizeRaceChoice(race, subrace) {
        const finalRace = mergeRaceAndSubrace(race, subrace);
        this.props.input.onChange(finalRace ? finalRace.id : null);
        this.props.onRaceSelectionComplete(finalRace);
    }

    findRaceById(selectedRaceId) {
        return this.props.races.find((race) => race.id === selectedRaceId);
    }

    render() {
        let {selectedRaceId, selectedSubraceId} = this.state;
        let availableSubraces;
        if (selectedRaceId !== null) {
            availableSubraces = this.props.races.filter((race) => race.parent === selectedRaceId);
        } else {
            availableSubraces = [];
        }
        let subraceSelector;
        if (availableSubraces.length > 0) {
            subraceSelector = (
                <div>
                    <label htmlFor="subrace" className="sr-only">Subrace</label>
                    <select name="subrace" className="form-control form-control-sm" value={selectedSubraceId || ''}
                            disabled={availableSubraces.length === 0} onChange={this.handleSubraceSelectChange}>
                        {generateListOfRaceOptions(availableSubraces)}
                    </select>
                </div>
            );
        }

        return (
            <div className="card card-block">
                <h4 className="card-title font-weight-normal">Race</h4>

                <label htmlFor="race" className="sr-only">Race</label>
                <select name="race" className="form-control" value={selectedRaceId || ''}
                        onChange={this.handleRaceSelectChange}>
                    <option/>
                    {generateListOfRaceOptions(this.props.races.filter(race => !race.parent))}
                </select>
                {subraceSelector}
            </div>
        );
    }
}


RaceSelector.propTypes = {
    races: PropTypes.arrayOf(PropTypes.shape({})),
    onRaceSelectionComplete: PropTypes.func.isRequired
};

export default RaceSelector;
