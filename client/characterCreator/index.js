import {combineReducers} from 'redux';

import backgrounds from './module/backgrounds'
import characterOptions from './module/characterOptions'
import classes from './module/classes'
import races from './module/races'

export const reducer = combineReducers({
    backgrounds,
    classes,
    races,
    characterOptions
});