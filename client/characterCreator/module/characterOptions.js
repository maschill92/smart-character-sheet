const RACE_SELECTED = 'characterSheet/characterOptions/RACE_SELECTED';

const reducer = (state = {}, action) => {
    switch(action.type) {
        case RACE_SELECTED:
        default:
            return state;
    }
};
export default reducer;

export const raceSelected = (race) => ({
    type: RACE_SELECTED,
    payload: race
});