const LOAD_BEGIN = 'characterSheet/races/LOAD_BEGIN';
const LOAD_SUCCESS = 'characterSheet/races/LOAD_SUCCESS';

const initialState = {
    list: [],
    isLoaded: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case LOAD_BEGIN:
            return {
                list: [],
                isLoaded: false
            };
        case LOAD_SUCCESS:
            return {
                list: [...action.payload],
                isLoaded: true
            };
        default:
            return state;
    }
};
export default reducer;

export const loadRaces = () => {
    return (dispatch) => {
        dispatch(loadBegin());
        return $.get('/api/CharacterCreator/races', (races) => {
            dispatch(loadSuccess(races));
        });
    };
};

const loadBegin = () => {
    return {type: LOAD_BEGIN}
};

const loadSuccess = (races) => {
    return {type: LOAD_SUCCESS, payload: races};
};
