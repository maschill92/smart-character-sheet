const LOAD_BEGIN = 'characterSheet/classes/LOAD_BEGIN';
const LOAD_SUCCESS = 'characterSheet/classes/LOAD_SUCCESS';

const initialState = {
    list: [],
    isLoaded: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case LOAD_BEGIN:
            return {
                list: [],
                isLoaded: false
            };
        case LOAD_SUCCESS:
            return {
                list: [...action.payload],
                isLoaded: true
            };
        default:
            return state;
    }
};
export default reducer;

export const loadClasses = () => {
    return (dispatch) => {
        dispatch(loadBegin());
        return $.get('/api/CharacterCreator/classes', (classes) => {
            dispatch(loadSuccess(classes));
        });
    };
};

const loadBegin = () => {
    return {type: LOAD_BEGIN}
};

const loadSuccess = (classes) => {
    return {type: LOAD_SUCCESS, payload: classes};
};
