const LOAD_BEGIN = 'characterSheet/backgrounds/LOAD_BEGIN';
const LOAD_SUCCESS = 'characterSheet/backgrounds/LOAD_SUCCESS';

const initialState = {
    list: [],
    isLoaded: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case LOAD_BEGIN:
            return {
                list: [],
                isLoaded: false
            };
        case LOAD_SUCCESS:
            return {
                list: [...action.payload],
                isLoaded: true
            };
        default:
            return state;
    }
};
export default reducer;

export const loadBackgrounds = () => {
    return (dispatch) => {
        dispatch(loadBegin());
        return $.get('/api/CharacterCreator/backgrounds', (backgrounds) => {
            dispatch(loadSuccess(backgrounds));
        });
    }
};

const loadBegin = () => {
    return {type: LOAD_BEGIN}
};

const loadSuccess = (backgrounds) => {
    return {type: LOAD_SUCCESS, payload: backgrounds};
};
