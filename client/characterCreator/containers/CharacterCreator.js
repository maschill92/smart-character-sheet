import React from 'react'
import {connect} from 'react-redux'

import CharacterCreatorForm from './CharacterCreatorForm'

const TOTAL_STORES_TO_LOAD = 3;

const spinner = (value) => (
    <progress className="progress progress-striped progress-animated" value={value} max={TOTAL_STORES_TO_LOAD}/>
);

export const CharacterCreator = ({backgroundsIsLoaded, classesIsLoaded, racesIsLoaded}) => {
    let totalStoresLoaded = 0;
    let display;
    //noinspection FallThroughInSwitchStatementJS
    switch (true) {
        case backgroundsIsLoaded:
            totalStoresLoaded++;
        case classesIsLoaded:
            totalStoresLoaded++;
        case racesIsLoaded:
            totalStoresLoaded++;
    }

    if (totalStoresLoaded < TOTAL_STORES_TO_LOAD) {
        display = spinner(totalStoresLoaded);
    } else {
        display = (<CharacterCreatorForm/>);
    }

    return (
        <div className="container">
            <div className="row">
                <h2 className="text-xs-center">Create New Character</h2>
            </div>
            {display}
        </div>
    )
};


const mapStatesToProps = (state) => {
    return {
        racesIsLoaded: state.characterCreator.races.isLoaded,
        backgroundsIsLoaded: state.characterCreator.backgrounds.isLoaded,
        classesIsLoaded: state.characterCreator.classes.isLoaded
    }
};

export default connect(mapStatesToProps)(CharacterCreator);