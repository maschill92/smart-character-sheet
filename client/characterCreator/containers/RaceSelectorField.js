import React from 'react'
import {connect} from 'react-redux'
import {Field} from 'redux-form'
import RaceSelectorComponent from '../components/RaceSelector'
import {raceSelected} from '../module/characterOptions'

const RaceSelectorField = (props) => {
    return (
        <Field name="race" component={RaceSelectorComponent} {...props} />
    );
};

const mapStateToProps = (state) => {
    return {
        races: state.characterCreator.races.list
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onRaceSelectionComplete: (race) => {
            dispatch(raceSelected(race));
        }
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RaceSelectorField);
