import React, {PropTypes} from 'react';
import {reduxForm} from 'redux-form'

import BasicCharacterInformationForm from '../components/BasicCharacterInformation'
import RaceSelectorField from './RaceSelectorField'

const initialFormValues = {
    alignment: 'NN'
};

const validate = values => {
    const errors = {};
    if (!values.name) {
        errors.name = 'Required'
    }
    return errors;
};

const CharacterCreatorForm = () => (
    <form className="form">
        <div className="row">
            <div className="col-xs-12">
                <BasicCharacterInformationForm/>
            </div>
        </div>

        <div className="row">
            <div className="col-md-8">
                <RaceSelectorField/>
            </div>
        </div>

    </form>
);

const formWrapped = reduxForm({
    form: 'character',
    initialValues: initialFormValues,
    validate
})(CharacterCreatorForm);


export default formWrapped