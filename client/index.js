import './scss/app.scss'
import React from 'react'
import 'babel-polyfill'
import {render} from 'react-dom';
import {browserHistory} from 'react-router'
import {syncHistoryWithStore} from 'react-router-redux'
import Root from './containers/Root'
import configureStore, {loadInitialAsyncStoreData} from './configureStore'

const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

loadInitialAsyncStoreData(store);

render(
    <Root store={store} history={history}/>,
    document.getElementById('root')
);
