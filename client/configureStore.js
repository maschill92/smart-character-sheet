import {createStore, applyMiddleware, compose} from 'redux'
import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'
import DevTools from './containers/DevTools'
import rootReducer from './reducer'

import {loadBackgrounds} from './characterCreator/module/backgrounds'
import {loadClasses} from './characterCreator/module/classes'
import {loadRaces} from './characterCreator/module/races'

export const loadInitialAsyncStoreData = (store) => {
    store.dispatch(loadBackgrounds());
    store.dispatch(loadClasses());
    store.dispatch(loadRaces());
};

export default function configureStore(initialState) {
    return createStore(
        rootReducer,
        initialState,
        compose(
            applyMiddleware(
                thunkMiddleware,
                createLogger({collapsed: (getState, action) => action.type.indexOf('characterSheet/') < 0})
            ),
            DevTools.instrument()
        )
    );
}